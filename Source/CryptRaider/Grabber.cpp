// Fill out your copyright notice in the Description page of Project Settings.


#include "Grabber.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
//#include "Math/UnrealMathUtility.h"




// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();	
}


// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	

	Start = GetComponentLocation();
	End = Start + GetForwardVector() * MaxGrabDistance;
	DrawDebugLine(
		GetWorld(),
		Start,
		End,
		FColor::Red
	);

	UPhysicsHandleComponent* PhysicsHandle = GetPhysicsHandle();
	if(PhysicsHandle && PhysicsHandle->GetGrabbedComponent() != nullptr){
		// move the object once grabbed
		// at start from player, forward to cameraview * holddistance
		// rotate on component rotation
		PhysicsHandle->SetTargetLocationAndRotation(
			Start + GetForwardVector() * HoldDistance,
			GetComponentRotation()
		);
	}
	
}

void UGrabber::Release(){
	UPhysicsHandleComponent* PhysicsHandle = GetPhysicsHandle();
	if(PhysicsHandle && PhysicsHandle->GetGrabbedComponent() != nullptr){
		// remove tag from actor
		PhysicsHandle->GetGrabbedComponent()->GetOwner()->Tags.Remove("Grabbed");
		// wake up the component from sleeping
		PhysicsHandle->GetGrabbedComponent()->WakeAllRigidBodies();
		PhysicsHandle->ReleaseComponent();
		UE_LOG(LogTemp, Display, TEXT("Release grabber"));
	}
}

void UGrabber::Grab(){
	// handle for the physics
	UPhysicsHandleComponent* PhysicsHandle = GetPhysicsHandle();
	if(PhysicsHandle == nullptr){
		return;
	}
	/*
	generate a collision channel sphere projected from
	the player onward to End vector,
	returns a pointer on HitResult
	*/

	FHitResult HitResult;
	if(GetGrabbableInReach(HitResult)){		
		UPrimitiveComponent* HitComponent = HitResult.GetComponent();
		HitComponent->SetSimulatePhysics(true);
		HitResult.GetActor()->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		// wake up the component from sleeping
		HitComponent->WakeAllRigidBodies();
		// add a tag while it's grabbed to the actor
		HitResult.GetActor()->Tags.Add("Grabbed");

		// grab the component that was hit, no name for the inbonename
		// where to grab? at impact, which rotation? the component
		PhysicsHandle->GrabComponentAtLocationWithRotation(
			HitComponent,
			NAME_None,
			HitResult.ImpactPoint,
			GetComponentRotation()
		);
	}
}

UPhysicsHandleComponent* UGrabber::GetPhysicsHandle() const{
	UPhysicsHandleComponent* Result = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if(Result==nullptr){
		UE_LOG(LogTemp, Error, TEXT("Grabber requires a UPhysicsHandleComponent"));
	}
	return Result;
}

bool UGrabber::GetGrabbableInReach(FHitResult& OutHitResult) const{
	FCollisionShape Sphere = FCollisionShape::MakeSphere(GrabRadius);
	return GetWorld()->SweepSingleByChannel(
		OutHitResult,
		Start,
		End,
		FQuat::Identity,
		ECC_GameTraceChannel2,
		Sphere
	);
}

/*
debug sphere on hit
DrawDebugSphere(GetWorld(), HitResult.Location, 10, 10, FColor::Blue, false, 5);
DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, 10, 10, FColor::Yellow, false, 5);
UE_LOG(LogTemp, Display, TEXT("hit %s"), *HitResult.GetActor()->GetActorNameOrLabel());
*/

/*
	float Damage{0.f};
	PrintDamage(Damage);
	void UGrabber::PrintDamage(const float& Damage){
		UE_LOG(LogTemp, Display, TEXT("dmg %f"), Damage);
	}
*/

/*
rotation from camera
	FRotator MyRotation = GetComponentRotation();
	FString RotationString = MyRotation.ToCompactString();
	UE_LOG(LogTemp, Display, TEXT("Grabber rotation, %s"), *RotationString);
*/

/*
world time seconds
	UWorld* World = GetWorld();
	UE_LOG(LogTemp, Display, TEXT("Seconds %f"), World->GetTimeSeconds());
*/

