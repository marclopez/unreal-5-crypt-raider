// Fill out your copyright notice in the Description page of Project Settings.


#include "TriggerComponent.h"


// Sets default values for this component's properties
UTriggerComponent::UTriggerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UTriggerComponent::BeginPlay()
{
	Super::BeginPlay();	
}


// Called every frame
void UTriggerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);	
	
	AActor* Actor = GetAcceptableActor();
	if(Actor != nullptr){
		UE_LOG(LogTemp, Display, TEXT("open"));
		// get the primitive component to disable physics
		UPrimitiveComponent* Component = Cast<UPrimitiveComponent>(Actor->GetRootComponent());
		if(Component != nullptr){
			Component->SetSimulatePhysics(false);
		}
		// attach the actor to this component (the one that owns the trigger class)
		Actor->AttachToComponent(
			this,
			FAttachmentTransformRules::KeepWorldTransform
		);
		Mover->SetShouldMove(true);
		if(Actor->ActorHasTag("Player")){
			Mover->SetDirection(true);
		}else{
			Mover->SetDirection(false);
		}
	}
	/*
	else{
		Mover->SetShouldMove(false);
	}
	*/
}

AActor* UTriggerComponent::GetAcceptableActor() const{
	TArray<AActor*> Actors;
	GetOverlappingActors(Actors);
	for (AActor* Actor : Actors){
		if(Actor->ActorHasTag(UnlockTag) && !Actor->ActorHasTag("Grabbed")){
			return Actor;
		}
	}
	return nullptr;
}

void UTriggerComponent::SetMover(UMover* NewMover){
	Mover = NewMover;
}


/*
iterating an array of aactor*

for (int32 i = 0; i < Actors.Num(); i++)
{
	UE_LOG(LogTemp, Display, TEXT("overlapping %s"), *Actors[i]->GetActorNameOrLabel());
}
*/