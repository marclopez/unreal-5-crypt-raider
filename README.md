# Unreal 5 Crypt Raider



My second small game created (first one from scratch) with Unreal Engine 5 for the "Unreal 5.0 C++ Developer: Learn C++ and Make Video Games" course from GameDev.tv.

Using the "Medieval Dungeon" by Infuse Studio: https://www.unrealengine.com/marketplace/en-US/product/a5b6a73fea5340bda9b8ac33d877c9e2?sessionInvalidated=true
